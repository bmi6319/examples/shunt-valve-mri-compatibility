"""
Automatic Brain Shunt Valve Recognition. Feature Analysis.abs

Chose which experiment to run based on command line arguments
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import sklearn.metrics as le_me
import argparse

import utils



if __name__ == '__main__':


    #===== Parameter parsing
    parser = argparse.ArgumentParser(description='Automatic Brain Shunt Valve Recognition. Feature Analysis.' )
    parser.add_argument('pipeline', choices=['lbp', 'hog', 'cnn'], \
                                        help='choose the pipeline [lbp | hog | cnn  ]')
    parser.add_argument('-ne', dest='necnn', action='store_const', const=True, required=False, \
                        help='do not use the enhanced CNN')
    args = parser.parse_args()
    
    
    if args.pipeline == 'cnn':
        print('Run analysis using features derived from convolutional neural networks')
        if args.necnn:
            print('Standard CNN')
            featFile = './res/feat-mrg-CNN-paper.npz' 
        else:
            print('Enhanced CNN')
            featFile = './res/feat-mrg-CNN-otsu-paper.npz' 
    elif args.pipeline == 'lbp':
        print('Run analysis using features computed with Histogram of Oriented Gradients (LBP)')
        featFile = './res/feat-lbp-paper.npz'
    elif args.pipeline == 'hog':
        print('Run analysis using features computed with Histogram of Oriented Gradients (HOG)')
        featFile = './res/feat-hog-paper.npz'
    else:
        assert(False) # throw an error 

    #===== 

    # load features
    assert (os.path.exists(featFile))
    print('loading ', featFile)
    fileDic = np.load(featFile)
    # set up class names
    usedClasses = ['Strata II - NSC', 'Codman-Hakim', 'Sophysa Polaris - SPV', 'Codman CERTAS', 'Miethke proGAV']


    # Cross validation
    X = fileDic['X']
    y = fileDic['y']
    yPredictionArr, yPredProbArr = utils.crossValidation(X, y)


    # Compute confusion matrix
    confMat = le_me.confusion_matrix(y, yPredictionArr)
    accuracy = (np.trace(confMat)*1.0) /  np.sum( confMat.flatten() )
    print(le_me.classification_report(y, yPredictionArr, target_names=usedClasses))

    # Compute confidence intervals
    utils.bootStrapMetrics(y, yPredictionArr, dataRatio=0.8)

    # Compute statistical significance
    print('-'*20, 'One vs. all tests')
    utils.classSigTests(y, yPredProbArr, usedClasses)