# Automatic Brain Shunt Valve Recognition. Feature Analysis

This repository contains the supporting code for the manuscript:

>L Giancardo, O Arevalo, A Tenreiro, R Riascos and E Bonfante. MRI Compatibility: Automatic Brain Shunt Valve Recognition using Feature Engineering and Deep Convolutional Networks. Scientific Reports volume 8, Article number: 16052 (2018).
https://www.nature.com/articles/s41598-018-34164-6

__Background__
MRI is an imaging modality essential for the diagnosis, prognosis and tracking of several medical condition. However, people with implantable devices are likely to lack accurate information about the safety profile of their device. 

We want to develop a system to support radiologist to identify the safety profile of implanted device from X-ray images.

Such system could be tightly integrated with the current radiologist workflow and leverage modern machine learning (ML) algorithms able to identify and quantify image patterns and being continuously updated with newer devices entering the market. 

This could save a significant a significant amount of radiologist time, improve safety and increase the number of MRI-based medical decision.

__Goal__

Show the feasibility of a X-rays-based implanted device identification system for MRI safety with a pilot study of the automatic image recognition component. 

__Data Analysis (Task and Dataset)__

We will compare machine learning models based on feature engineering and feature learning (deep learning) on a total of 416 skull X-rays that included brain shunt valves images were collected from the Memorial Hermann Hospital. We grouped together different versions of the same brain shunt valves models. 
The specific 5 class grouping is as follows: Codman-Certas® (42), Codman-Hakim® (106), Miethke ProGAV® (22), Sophysa Polaris SPV® (standard/140/400) (82) and Medtronic Strata® (II/NSC) (164). All images were acquired from different subjects and were selected regardless of acquisition perspective, scale or any demographic information

![devices](imgs/devices.jpg)

__Results__

Our best performing method identified the valve type correctly 96% [CI 94-98%] of the times (CI: confidence intervals, precision 0.96, recall 0.96, f1-score 0.96), tested using a stratified cross-validation approach to avoid chances of overfitting.

![classifier comparison](imgs/classifier-comparison.png)    

__Conclusions__

We were able to establish feasibility of a X-rays-based implanted device identification system for MRI safety on brain shunt valves images. Additionally our results clearly indicate that feature learning techniques outperform the feature engineering approaches tested on this dataset.





## Installation

The feature analysis code can be installed and run in three different ways:
1. Binary Docker container on DockerHUB
2. By building the Docker image yourself
3. Local Python environment


## 1- Run the Binary Docker Container
A binary Docker image has already been created for you on DockerHUB https://hub.docker.com/r/lgianca/shunt-valve/. 
You can run it by installing docker on your machine and running:

```
docker run --rm lgianca/shunt-valve
```
or 
```
docker run --rm lgianca/shunt-valve python runExpCNN-paper.py
```

this will run the analysis using features derived from convolutional neural networks. The other features analysis pipelines can be run as:


```
docker run --rm lgianca/shunt-valve python runExpHOG-paper.py
```
for features computed with Histogram of Oriented Gradients (HOG)  

and 
```
docker run --rm lgianca/shunt-valve python runExpLBP-paper.py
```
for features computed with Local Binary Patterns (LBP)  


## 2- Building the Docker image 
### Installation

Build Docker image with
```
docker build -t shunt-valve .
```

### Run local Docker container
```
docker run shunt-valve python runExpCNN-paper.py
```

the other features analysis pipelines can be run as shown in method 1

## 3- Local Python Environment
### Installation 
This code has been tested with Python 3.6 and pip 

```
git clone https://gitlab.com/bmi6319-spring2019/examples/shunt-valve-mri-compatibility
cd shunt-valve-mri-compatibility
cd src
```
Optionally, create a virtual environment with
```
conda create -n valve-env python=3.6 
```
activate it
```
source activate valve-env
```
or
```
conda activate valve-env
```

and finally, install the required dependencies
```
pip install -r requirements.txt
```

### Run 
Run analysis using features derived from convolutional neural networks  
```
python runExpCNN-paper.py
```
Run analysis using features computed with Histogram of Oriented Gradients (HOG)  
```
python runExpHOG-paper.py
```
Run analysis using features computed with Local Binary Patterns (LBP)  
```
python runExpLBP-paper.py
```


## Other information
This code is free to use for any non-commercial purpose provided that the original publication is cited. 

We refer to the original publication for additional infomation and acknowledgements.
