FROM python:3.6-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY src /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# update tk for matplotly library
RUN apt-get update && apt-get install -y tk

# copy readme displaying initialization message
COPY readme-docker.txt /app

# Default command
CMD cat readme-docker.txt && python runExpCNN-paper.py

# build with
# docker build -t shunt-valve .

# run with
# docker run --rm shunt-valve
